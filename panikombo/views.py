from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import JsonResponse
from emissions.models import Episode, SoundFile


@login_required
def soundfiles(request, *args, **kwargs):
    terms = kwargs.get('term', request.GET.get('term', ''))
    queryset = SoundFile.objects.published()
    for term in terms.split():
        queryset = queryset.filter(
            Q(title__icontains=term)
            | Q(episode__title__icontains=term)
            | Q(episode__emission__title__icontains=term)
        )

    def fmt(soundfile):
        return '%s - %s - %s' % (
            soundfile.episode.emission.title,
            soundfile.episode.title,
            soundfile.title or soundfile.id,
        )

    return JsonResponse(
        {
            'results': [
                {
                    'text': fmt(x),
                    'id': x.pk,
                }
                for x in queryset
            ],
            'more': False,
        }
    )


@login_required
def episodes(request, *args, **kwargs):
    terms = kwargs.get('term', request.GET.get('term', ''))
    queryset = Episode.objects.all()
    for term in terms.split():
        queryset = queryset.filter(Q(title__icontains=term) | Q(emission__title__icontains=term))

    def fmt(episode):
        return '%s - %s' % (episode.emission.title, episode.title)

    return JsonResponse(
        {
            'results': [
                {
                    'text': fmt(x),
                    'id': x.pk,
                }
                for x in queryset
            ],
            'more': False,
        }
    )
