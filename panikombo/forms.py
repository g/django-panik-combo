from django import forms
from django.forms.widgets import Widget
from emissions.models import Episode, SoundFile
from taggit.forms import TagWidget

from .models import EpisodeAutoSelectionCell, EpisodeCell, NewsItemAutoSelectionCell, SoundCell


class BaseSelect2Widget(Widget):
    template_name = "panikombo/select2.html"

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['view_name'] = self.view_name
        if value:
            context['widget']['view_value'] = self.view_value(value)
        return context


class SoundFileWidget(BaseSelect2Widget):
    view_name = 'panikombo-select2-soundfiles'

    def view_value(self, value):
        try:
            soundfile = SoundFile.objects.get(id=value)
        except SoundFile.DoesNotExist:
            return 'missing sound %s' % value
        return '%s - %s - %s' % (
            soundfile.episode.emission.title,
            soundfile.episode.title,
            soundfile.title or soundfile.id,
        )


class SoundCellForm(forms.ModelForm):
    class Meta:
        model = SoundCell
        fields = ('soundfile',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['soundfile'].widget = SoundFileWidget()


class EpisodeWidget(BaseSelect2Widget):
    view_name = 'panikombo-select2-episodes'

    def view_value(self, value):
        try:
            episode = Episode.objects.get(id=value)
        except Episode.DoesNotExist:
            return 'missing episode %s' % value
        return '%s - %s' % (episode.emission.title, episode.title)


class EpisodeCellForm(forms.ModelForm):
    class Meta:
        model = EpisodeCell
        fields = ('episode',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['episode'].widget = EpisodeWidget()


class EpisodeAutoSelectionCellForm(forms.ModelForm):
    class Meta:
        model = EpisodeAutoSelectionCell
        fields = ('title', 'tags', 'and_tags', 'category', 'period')
        widgets = {'tags': TagWidget()}


class NewsItemAutoSelectionCellForm(forms.ModelForm):
    class Meta:
        model = NewsItemAutoSelectionCell
        fields = ('tags', 'and_tags', 'category', 'future', 'count')
        widgets = {'tags': TagWidget()}
