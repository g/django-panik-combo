import os

from combo.apps.gallery.models import GalleryCell, Image
from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('page', metavar='SLUG', type=str)
        parser.add_argument('directory', metavar='DIR', type=str)

    def handle(self, page, directory, *args, **kwargs):
        gallery = GalleryCell.objects.filter(page__snapshot__isnull=True, page__slug=page).first()
        for i, filename in enumerate(sorted(os.listdir(directory))):
            with open(os.path.join(directory, filename), 'rb') as fd:
                image = Image(gallery=gallery, order=i, image=ContentFile(fd.read(), name=filename))
                image.save()
