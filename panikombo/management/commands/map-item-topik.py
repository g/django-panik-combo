from combo.data.models import CellBase, Page
from django.core.management.base import BaseCommand, CommandError
from emissions.models import Episode, NewsItem

from panikombo.models import ItemTopik


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        remaining_item_topik = set(ItemTopik.objects.all())
        for topik_page in Page.objects.exclude(picture__isnull=True):
            cells = CellBase.get_cells(page=topik_page)
            for cell in cells:
                if not hasattr(cell, 'get_included_items'):
                    continue
                for item in cell.get_included_items():
                    kwargs = {'page': topik_page}
                    if isinstance(item, NewsItem):
                        kwargs['newsitem'] = item
                    elif isinstance(item, Episode):
                        kwargs['episode'] = item
                    else:
                        continue
                    obj, created = ItemTopik.objects.get_or_create(**kwargs)
                    if obj in remaining_item_topik:
                        remaining_item_topik.remove(obj)
        for item_topik in remaining_item_topik:
            item_topik.delete()
