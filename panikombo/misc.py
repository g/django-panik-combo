import collections

COMBO_PUBLIC_TEMPLATES = collections.OrderedDict(
    {
        'standard': {
            'name': 'Page avec barre latérale',
            'template': 'combo/page_template_sidebar.html',
            'placeholders': collections.OrderedDict(
                {
                    'introduction': {
                        'name': 'Introduction',
                        'optional': True,
                    },
                    'content': {
                        'name': 'Contenu',
                    },
                    'sidebar': {
                        'name': 'Barre latérale',
                    },
                }
            ),
        },
        'fullwidth': {
            'name': 'Page prenant toute la largeur',
            'template': 'combo/page_template_full_width.html',
            'placeholders': {
                'introduction': {
                    'name': 'Introduction',
                    'optional': True,
                },
                'content': {
                    'name': 'Contenu',
                },
            },
        },
        'standard-about': {
            'name': 'Section « À propos »',
            'template': 'combo/page_template_about.html',
            'placeholders': {
                'content': {
                    'name': 'Contenu',
                },
            },
        },
    }
)

COMBO_CELL_TEMPLATES = {
    'data_menucell': {
        'horizontal': {
            'label': 'Horizontal',
            'extra-css-classes': 'menu-horizontal',
        },
        'vertical': {
            'label': 'Vertical',
            'extra-css-classes': 'menu-vertical',
        },
        'topiks': {
            'label': 'Topiks',
            'template': 'cells/menu-topiks.html',
        },
    },
    'panikombo_weekprogramcell': {
        'grid': {
            'label': 'Affichage en grille',
            'template': 'panikombo/week_program_grid.html',
        },
    },
}
