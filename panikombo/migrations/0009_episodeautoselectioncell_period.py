from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('panikombo', '0008_topikcell'),
    ]

    operations = [
        migrations.AddField(
            model_name='episodeautoselectioncell',
            name='period',
            field=models.PositiveSmallIntegerField(
                default=0, verbose_name='Period', choices=[(0, 'All'), (1, 'Future'), (2, 'Past')]
            ),
            preserve_default=True,
        ),
    ]
