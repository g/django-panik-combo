from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('panikombo', '0009_episodeautoselectioncell_period'),
    ]

    operations = [
        migrations.AddField(
            model_name='episodeautoselectioncell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='episodecell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='newsitemautoselectioncell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='soundcell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='topikcell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
    ]
