import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0009_auto_20150529_2247'),
        ('panikombo', '0007_newsitemautoselectioncell_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='TopikCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('text', ckeditor.fields.RichTextField(null=True, verbose_name='Text', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Groups', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.SET_NULL)),
                ('topik', models.ForeignKey(to='panikombo.Topik', null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'verbose_name': 'Topik',
            },
            bases=(models.Model,),
        ),
    ]
