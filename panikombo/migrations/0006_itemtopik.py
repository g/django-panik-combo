from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0004_focus_page'),
        ('panikombo', '0005_topik'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemTopik',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'episode',
                    models.ForeignKey(
                        verbose_name='Episode',
                        blank=True,
                        to='emissions.Episode',
                        null=True,
                        on_delete=models.SET_NULL,
                    ),
                ),
                (
                    'newsitem',
                    models.ForeignKey(
                        verbose_name='News Item',
                        blank=True,
                        to='emissions.NewsItem',
                        null=True,
                        on_delete=models.SET_NULL,
                    ),
                ),
                (
                    'topik',
                    models.ForeignKey(
                        verbose_name=b'Topik',
                        blank=True,
                        to='panikombo.Topik',
                        null=True,
                        on_delete=models.SET_NULL,
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]
