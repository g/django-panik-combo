from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('panikombo', '0033_cells_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emissionscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='episodeautoselectioncell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='episodecell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='focuscarrouselcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='newsitemautoselectioncell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='soundcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='soundscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='weekprogramcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
