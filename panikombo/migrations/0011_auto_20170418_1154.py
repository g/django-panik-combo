import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('panikombo', '0010_auto_20160220_1153'),
    ]

    operations = [
        migrations.AddField(
            model_name='episodeautoselectioncell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='episodeautoselectioncell',
            name='last_update_timestamp',
            field=models.DateTimeField(
                default=datetime.datetime(2017, 4, 18, 11, 54, 35, 51768), auto_now=True
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='episodecell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='episodecell',
            name='last_update_timestamp',
            field=models.DateTimeField(
                default=datetime.datetime(2017, 4, 18, 11, 54, 36, 700929), auto_now=True
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='newsitemautoselectioncell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='newsitemautoselectioncell',
            name='last_update_timestamp',
            field=models.DateTimeField(
                default=datetime.datetime(2017, 4, 18, 11, 54, 39, 362967), auto_now=True
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='soundcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='soundcell',
            name='last_update_timestamp',
            field=models.DateTimeField(
                default=datetime.datetime(2017, 4, 18, 11, 54, 42, 662255), auto_now=True
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='topikcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='topikcell',
            name='last_update_timestamp',
            field=models.DateTimeField(
                default=datetime.datetime(2017, 4, 18, 11, 54, 44, 42144), auto_now=True
            ),
            preserve_default=False,
        ),
    ]
