import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in [
        'SoundCell',
        'EpisodeCell',
        'EpisodeAutoSelectionCell',
        'NewsItemAutoSelectionCell',
        'SoundsCell',
        'WeekProgramCell',
        'FocusCarrouselCell',
        'EmissionsCell',
    ]:
        klass = apps.get_model('panikombo', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('panikombo', '0032_cells_uuid'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
