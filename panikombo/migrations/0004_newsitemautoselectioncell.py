import taggit.managers
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0005_auto_20150226_0903'),
        ('taggit', '0001_initial'),
        ('panikombo', '0003_episodeautoselectioncell'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsItemAutoSelectionCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('title', models.CharField(max_length=50, verbose_name='Title', blank=True)),
                ('future', models.BooleanField(default=True, verbose_name='Future Events Only')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Groups', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.SET_NULL)),
                (
                    'tags',
                    taggit.managers.TaggableManager(
                        to='taggit.Tag',
                        through='taggit.TaggedItem',
                        blank=True,
                        help_text='A comma-separated list of tags.',
                        verbose_name='Tags',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Automatic Newsitem Selection',
            },
            bases=(models.Model,),
        ),
    ]
