# Generated by Django 1.11.29 on 2020-11-21 20:58

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('panikombo', '0017_soundscell_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='soundscell',
            name='sort_order',
            field=models.CharField(
                choices=[
                    ('-creation_timestamp', 'Reverse chronological (creation)'),
                    ('-first_diffusion', 'Reverse chronological (diffusion)'),
                    ('creation_timestamp', 'Chronological (creation)'),
                    ('first_diffusion', 'Chronological (diffusion)'),
                    ('?', 'Random'),
                ],
                default='-creation_timestamp',
                max_length=30,
                verbose_name='Sort order',
            ),
        ),
    ]
