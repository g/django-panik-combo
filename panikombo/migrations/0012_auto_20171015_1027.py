from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('panikombo', '0011_auto_20170418_1154'),
    ]

    operations = [
        migrations.AddField(
            model_name='episodeautoselectioncell',
            name='and_tags',
            field=models.CharField(max_length=100, verbose_name='And Tags', blank=True),
        ),
        migrations.AddField(
            model_name='newsitemautoselectioncell',
            name='and_tags',
            field=models.CharField(max_length=100, verbose_name='And Tags', blank=True),
        ),
    ]
