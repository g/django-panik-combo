from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0005_auto_20150226_0903'),
        ('panikombo', '0004_newsitemautoselectioncell'),
    ]

    operations = [
        migrations.CreateModel(
            name='Topik',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('image', models.ImageField(max_length=250, null=True, verbose_name='Image', blank=True)),
                ('got_focus', models.DateTimeField(default=None, null=True, blank=True)),
                ('has_focus', models.BooleanField(default=False)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]
