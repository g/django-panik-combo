from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0004_focus_page'),
        ('panikombo', '0006_itemtopik'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsitemautoselectioncell',
            name='category',
            field=models.ForeignKey(
                verbose_name='Category',
                blank=True,
                to='emissions.NewsCategory',
                null=True,
                on_delete=models.SET_NULL,
            ),
            preserve_default=True,
        ),
    ]
