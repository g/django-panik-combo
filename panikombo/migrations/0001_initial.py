from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('emissions', '0001_initial'),
        ('data', '0005_auto_20150226_0903'),
    ]

    operations = [
        migrations.CreateModel(
            name='SoundCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Groups', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.SET_NULL)),
                (
                    'soundfile',
                    models.ForeignKey(to='emissions.SoundFile', null=True, on_delete=models.SET_NULL),
                ),
            ],
            options={
                'verbose_name': 'Sound',
            },
            bases=(models.Model,),
        ),
    ]
