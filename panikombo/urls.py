from django.urls import path

from . import views

urlpatterns = [
    path('select2/episodes/', views.episodes, name='panikombo-select2-episodes'),
    path('select2/soundfiles/', views.soundfiles, name='panikombo-select2-soundfiles'),
]
